import request from '../utils/request';

// import axios from 'axios';
 
// const instance = axios.create({
//   timeout: 10000,
//    headers: {
//     'Content-Type': "application/json;charset=utf-8"
//   }
// })

export const fetchData = query => {
    return request({
        url: './table.json',
        method: 'get',
        params: query
    });
};

// export default {
//     updatePermissions ( data ) {
//       return instance.post('/role/rolePermission', data);
//     },
   
// }