import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/geo'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/dashboard',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Dashboard.vue'),
                    meta: { title: '系统首页' }
                },
                {
                    path: '/icon',
                    component: () => import(/* webpackChunkName: "icon" */ '../components/page/Icon.vue'),
                    meta: { title: '自定义图标' }
                },
                {
                    path: '/table',
                    component: () => import(/* webpackChunkName: "table" */ '../components/page/BaseTable.vue'),
                    meta: { title: '基础表格' }
                },
                {
                    path: '/tabs',
                    component: () => import(/* webpackChunkName: "tabs" */ '../components/page/Tabs.vue'),
                    meta: { title: 'tab选项卡' }
                },
                {
                    path: '/form',
                    component: () => import(/* webpackChunkName: "form" */ '../components/page/BaseForm.vue'),
                    meta: { title: '基本表单' }
                },
                {
                    // 富文本编辑器组件
                    path: '/editor',
                    component: () => import(/* webpackChunkName: "editor" */ '../components/page/VueEditor.vue'),
                    meta: { title: '富文本编辑器' }
                },
                {
                    // markdown组件
                    path: '/markdown',
                    component: () => import(/* webpackChunkName: "markdown" */ '../components/page/Markdown.vue'),
                    meta: { title: 'markdown编辑器' }
                },
                {
                    // 图片上传组件
                    path: '/upload',
                    component: () => import(/* webpackChunkName: "upload" */ '../components/page/Upload.vue'),
                    meta: { title: '文件上传' }
                },
                {
                    // vue-schart组件
                    path: '/charts',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/page/BaseCharts.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // echartS组件
                    path: '/chartss',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/display.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // geo组件
                    path: '/geo',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/geo.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // 报表概况组件
                    path: '/gaikuang',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/gaikuang.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // 销售概况组件关于会计股东
                    path: '/xiaoshou',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/xiaoshou.vue'),
                    meta: { title: '销售概况' }
                },
                {
                    // 销售概况组件关于分销商
                    path: '/xiaoshoufen',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/xiaoshoufen.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // 销售概况组件关于医院对接人
                    path: '/xiaoshouyyDjr',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/xiaoshouyyDjr.vue'),
                    meta: { title: 'schart图表' }
                },
                {
                    // 销售概况组件关于科室对接人
                    path: '/xiaoshoukeDjr',
                    component: () => import(/* webpackChunkName: "chart" */ '../components/baobiao/xiaoshoukeDjr.vue'),
                    meta: { title: 'schart图表' }
                },

                {
                    // 拖拽列表组件
                    path: '/drag',
                    component: () => import(/* webpackChunkName: "drag" */ '../components/page/DragList.vue'),
                    meta: { title: '拖拽列表' }
                },
                {
                    // 拖拽Dialog组件
                    path: '/dialog',
                    component: () => import(/* webpackChunkName: "dragdialog" */ '../components/page/DragDialog.vue'),
                    meta: { title: '拖拽弹框' }
                },
                {
                    // 国际化组件
                    path: '/i18n',
                    component: () => import(/* webpackChunkName: "i18n" */ '../components/page/I18n.vue'),
                    meta: { title: '国际化' }
                },
                {
                    // 权限页面
                    path: '/permissions',
                    component: () => import(/* webpackChunkName: "permission" */ '../components/page/Permission.vue'),
                    meta: { title: '权限测试', permissions: true }
                },
                {
                    path: '/404',
                    component: () => import(/* webpackChunkName: "404" */ '../components/page/404.vue'),
                    meta: { title: '404' }
                },
                {
                    path: '/403',
                    component: () => import(/* webpackChunkName: "403" */ '../components/page/403.vue'),
                    meta: { title: '403' }
                },
                {
                    path: '/donate',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/page/Donate.vue'),
                    meta: { title: '支持作者' }
                },
                {
                    path: '/bedinfo',
                    component: () => import(/* webpackChunkName: "bedinfo" */ '../components/bedPage/bedinfo.vue'),
                    meta: { title: '床位信息' }   
                },
                {
                    path: '/malfunction',
                    component: () => import(/* webpackChunkName: "malfunction" */ '../components/malfunctionpage/malfunction.vue'),
                    meta: { title: '故障列表' }   
                },

                {
                    path: '/malfunctioninfo',
                    name:'malfunctioninfo',
                    component: () => import(/* webpackChunkName: "malfunctioninfo" */ '../components/malfunctionpage/malfunctioninfo.vue'),
                    meta: { title: '故障详情' }
                },
                {
                    path: '/maintain',
                    component: () => import(/* webpackChunkName: "maintain" */ '../components/maintainpage/maintain.vue'),
                    meta: { title: '维修列表' }   
                },

                {
                    path: '/maintaininfo',
                    name:'maintaininfo',
                    component: () => import(/* webpackChunkName: "maintaininfo" */ '../components/maintainpage/maintaininfo.vue'),
                    meta: { title: '维修列表' }   
                },

                
                {
                    path: '/bedinfoitem1',
                    name:'bedinfoitem1',
                    component: () => import(/* webpackChunkName: "bedinfoitem1" */ '../components/bedPage/bedinfoitem1.vue'),
                    meta: { title: '床位详情' }
                },
                {
                    path: '/hospital',
                    component: () => import(/* webpackChunkName: "hospital" */ '../components/hospital/Hospital.vue'),
                    meta: { title: '医院信息' }
                },
                {
                    path: '/hospitalMessage',
                    component: () => import(/* webpackChunkName: "hospital" */ '../components/hospital/HospitalMessage.vue'),
                    meta: { title: '医院详情' }
                },
                {
                    path: '/department',
                    component: () => import(/* webpackChunkName: "hospital" */ '../components/hospital/Department.vue'),
                    meta: { title: '科室信息' }
                },
                {
                    path: '/departmentMessage',
                    component: () => import(/* webpackChunkName: "hospital" */ '../components/hospital/DepartmentMessage.vue'),
                    meta: { title: '科室详情' }
                },
                {

                    path: '/shareholder',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/han/shareholder.vue'),
                    meta: { title: '股东/经销商管理' }
                },
                {
                    path: '/shareholderInfo',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/han/shareholderInfo.vue'),
                    meta: { title: '股东详情' }
                },

                {
                    path: '/weixiu',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/han/weixiu.vue'),
                    meta: { title: '维修人员管理' }
                },
                {
                    path: '/weixiurenInfo',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/han/weixiurenInfo.vue'),
                    meta: { title: '维修人员详情' }
                },
                {
                    path: '/kuaiji',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/han/kuaiji.vue'),
                    meta: { title: '会计管理' }
                },
                {
                    path: '/duijieren',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/han/duijieren.vue'),
                    meta: { title: '对接人管理' }
                },
                {
                    // 用户列表
                    path: '/showusers',
                    component: () => import(/* webpackChunkName: "showusers" */ '../components/user/User.vue'),
                    meta: { title: '用户列表' }
                },
                {
                    // 用户编辑
                    path: '/edituser/:id',
                    component: () => import(/* webpackChunkName: "edituser" */ '../components/user/UserEdit.vue'),
                    meta: { title: '用户编辑' }
                },
                {
                    //订单管理
                    path: '/showOrders',
                    component: () => import(/* webpackChunkName: "showOrders" */ '../components/user/RentBed.vue'),
                    meta: { title: '订单管理' }
                },
                {
                    //订单详情
                    path: '/orderDetail/:id',
                    component: () => import(/* webpackChunkName: "orderDetail" */ '../components/user/RentBedDetail.vue'),
                    meta: { title: '订单详情' }
                },
                {
                    path: '/role',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/setting/rolemain.vue'),
                    meta: { title: '角色管理' }
                },
                {
                    path: '/permission',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/setting/permission.vue'),
                    meta: { title: '权限管理' }
                },
                {
                    path: '/message',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/setting/message.vue'),
                    meta: { title: '信息管理' }
                },
                {
                    path: '/logs',
                    component: () => import(/* webpackChunkName: "donate" */ '../components/setting/logs.vue'),
                    meta: { title: '日志管理' }
                },
                // {
                //     path: '/message',
                //     component: () => import(/* webpackChunkName: "donate" */ '../components/setting/message.vue'),
                //     meta: { title: '信息管理' }
                // },

               
            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
