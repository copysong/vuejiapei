module.exports = {
    baseUrl: './',
    assetsDir: 'static',
    productionSourceMap: false,
    devServer: {
        proxy: {
            '/':{
                target:'http://localhost:8081/',
                changeOrigin:true,
                pathRewrite:{
                    '/':''
                }
            }
        }
    }
}